package com.example.franciscojavier.farmurself.Adaptadores;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.example.franciscojavier.farmurself.InfoCultivos.InfoCultActivity;
import com.example.franciscojavier.farmurself.R;

public class HeaderAdapter extends BaseAdapter {

    private final Context mContext;

    public HeaderAdapter(Context mContext) {
        this.mContext = mContext;


    }


    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup){
        if (view == null){
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.grid_header, viewGroup, false);
        }
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, InfoCultActivity.class);
                mContext.startActivity(intent);
            }
        });

        /*Cultivo item = getItem(position);

        //Setear la imagen
        ImageView image = (ImageView) view.findViewById(R.id.imagen);
        Glide.with(image.getContext()).load(item.getIdImage()).into(image);

        //Setear el nombre
        TextView name = (TextView)view.findViewById(R.id.nombre);
        name.setText(item.getNombre());

        //Setear el detalle
        TextView detalle = (TextView)view.findViewById(R.id.detalle);
        detalle.setText(item.getDetalle()); */

        return view;
    }
}
