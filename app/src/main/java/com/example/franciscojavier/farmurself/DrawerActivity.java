package com.example.franciscojavier.farmurself;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.franciscojavier.farmurself.Clases.Utilidades;
import com.example.franciscojavier.farmurself.InfoCultivos.InfoAlba;
import com.example.franciscojavier.farmurself.InfoCultivos.InfoCibou;
import com.example.franciscojavier.farmurself.InfoCultivos.InfoCultActivity;
import com.example.franciscojavier.farmurself.MenuDrawerFragments.ajustesFragment;
import com.example.franciscojavier.farmurself.MenuDrawerFragments.ayudaFragment;
import com.example.franciscojavier.farmurself.MenuDrawerFragments.historialFragment;
import com.facebook.login.LoginManager;
import com.github.clans.fab.FloatingActionMenu;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class DrawerActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, ajustesFragment.OnFragmentInteractionListener, ayudaFragment.OnFragmentInteractionListener, historialFragment.OnFragmentInteractionListener, ContenedorFragment.OnFragmentInteractionListener{


    PagerAdapter mPageAdapter;
    ViewPager mViewPager;
    public FragmentTabHost tabHost;
    TextView textUser;
    FloatingActionMenu btnFlo;
    FloatingActionButton fab1,fab2,fab3;
    com.github.clans.fab.FloatingActionButton btnAgregar, btnNuevo;
    boolean isFABOpen;
    public static JSONArray ja;
    Bundle bundle2 = new Bundle();
   public int ini;
    String nombreCulti1;
    String nombreCulti2;
    String nombreCulti3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


           // consultarCultivo("http:/192.168.43.41/listarcultivo.php");


          // Toast.makeText(this, txtNam, Toast.LENGTH_SHORT).show();
            Fragment frag = null;
            frag = new ContenedorFragment();
            frag.setArguments(bundle2);
            getSupportFragmentManager().beginTransaction().replace(R.id.coordinator, frag).commit();

            Utilidades.ValidarPantalla = false;



        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab1 = (FloatingActionButton) findViewById(R.id.fab1);
        fab2 = (FloatingActionButton) findViewById(R.id.fab2);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isFABOpen){
                    showFABMenu();
                }else{
                    closeFABMenu();
                }
            }
        });

        fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DrawerActivity.this, Escaner.class);
                startActivity(intent);
                finish();
            }
        });
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DrawerActivity.this, AgregarNuevoCultivo.class);
               startActivity(intent);
            }
        });











        /**  if (AccessToken.getCurrentAccessToken()==null){
         IrALogin();
         }else {
         Profile profile = Profile.getCurrentProfile();
         textUser.setText(profile.getName());
         // imagenUsuario.setProfileId(profile.getId());
         } **/

        /** btnCerrar.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
        LoginManager.getInstance().logOut();
        IrALogin();
        }
        }); **/
    }

    private void showFABMenu() {

        isFABOpen=true;
        fab1.animate().translationY(-getResources().getDimension(R.dimen.standard_55));
        fab2.animate().translationY(-getResources().getDimension(R.dimen.standard_105));

    }
    private void closeFABMenu(){
        isFABOpen=false;
        fab1.animate().translationY(0);
        fab2.animate().translationY(0);

    }


    @Override
    public void onBackPressed() {
        int count = getFragmentManager().getBackStackEntryCount();

        if (count == 0) {
            super.onBackPressed();
            getFragmentManager().popBackStack();
        } else {
            getFragmentManager().popBackStack();
        }
        if(!isFABOpen){
            super.onBackPressed();
        }else{
            closeFABMenu();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        Fragment miFragment = null;
        boolean fragmentSelect = false;

        if (id == R.id.nav_home) {
            miFragment = new ContenedorFragment();
            fragmentSelect = true;
        } else if (id == R.id.nav_list) {
            miFragment = new historialFragment();
            fragmentSelect = true;
        } else if (id == R.id.nav_ajustes) {
            miFragment = new ajustesFragment();
            fragmentSelect = true;
            //Intent intent = new Intent(this, SettingsActivity.class);
            //startActivity(intent);
        }

        if (fragmentSelect = true){
            getSupportFragmentManager().beginTransaction().replace(R.id.coordinator,miFragment).addToBackStack(null).commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    private void IrALogin() {

        Intent intent = new Intent(DrawerActivity.this, MainActivity.class);
        finish();
        startActivity(intent);
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            //Poner iconos
            ab.setDisplayHomeAsUpEnabled(true);
        }


    }
    private void setupViewPager(ViewPager viewPager) {
        DrawerActivity.SectionsPagerAdapter adapter = new DrawerActivity.SectionsPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(FragmentMainMenu.newInstance(1), "Mis Cultivos");
        adapter.addFragment(FragmentMainMenu.newInstance(2), "Listado de cultivos");

        viewPager.setAdapter(adapter);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    public void Cerrar(MenuItem item) {
        LoginManager.getInstance().logOut();
        IrALogin();
    }


    public void InfoClass(View view) {

        Intent intent = new Intent(this, InfoCultActivity.class);
        startActivity(intent);

    }

    public void InfoCibouClass(View view) {
        Intent intent = new Intent(this, InfoCibou.class);
        startActivity(intent);
    }

    public void InfoAlba(View view) {
        Intent intent = new Intent(this, InfoAlba.class);
        startActivity(intent);
    }


    public class SectionsPagerAdapter extends FragmentPagerAdapter {
    private final List<Fragment> mFragments = new ArrayList<>();
    private final List<String> mFragmentTitles = new ArrayList<>();

    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position);
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }

    public void addFragment(Fragment fragment, String title) {
        mFragments.add(fragment);
        mFragmentTitles.add(title);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitles.get(position);
    }
}
    private void consultarCultivo(String URL) {
        Log.i("url", "" + URL);
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    ja = new JSONArray(response);
                    nombreCulti1 = ja.getString(0);
                    //Toast.makeText(DrawerActivity.this, nombreCulti1, Toast.LENGTH_SHORT).show();




                } catch (JSONException e) {
                    Toast.makeText(DrawerActivity.this, "csm", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(stringRequest);
    }



}

