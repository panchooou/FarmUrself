package com.example.franciscojavier.farmurself.Clases;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.franciscojavier.farmurself.Adaptadores.HeadAdapter;
import com.example.franciscojavier.farmurself.R;

public class PanelActivity extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private HeadAdapter mAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_panel);

            mRecyclerView = (RecyclerView) findViewById(R.id.rv);

        mRecyclerView.setHasFixedSize(true);

        // Nuestro RecyclerView usará un linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        // Asociamos un adapter (ver más adelante cómo definirlo)
        //mAdapter = new HeadAdapter(myDataSet);
        mRecyclerView.setAdapter(mAdapter);

    }
}
