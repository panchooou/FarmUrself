package com.example.franciscojavier.farmurself;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.franciscojavier.farmurself.Clases.Cultivo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

public class ActivityUsuario extends AppCompatActivity {
    JSONArray ja;
    String name;
    TextView p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18;
    TextView fechaI, fechaF, nombre;
    ImageView imgGe;
    JsonObjectRequest jsonObjectRequest1;
    CheckBox rdy, rdy1, rdy2, rdy3;
    RequestQueue requestQueue;
    String paso1, paso2, paso3, paso4, paso5, paso6, paso7, paso8, paso9, paso10, paso11, paso12, paso13, paso14, paso15, paso16, paso17, paso18;
    String fechaIn, fechaFn, nombreCulti, fi, fn;
    int IdNoti;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usuario);

        requestQueue = Volley.newRequestQueue(getApplicationContext());

        Bundle obtiene = this.getIntent().getExtras();
        name = obtiene.getString("KY");
        IdNoti = obtiene.getInt("ID");

        imgGe = (ImageView) findViewById(R.id.imgGermi1);
        rdy = (CheckBox) findViewById(R.id.rdy);
        rdy1 = (CheckBox) findViewById(R.id.rdy1);


        if (IdNoti == 001){
            rdy.setText("Su cultivo debe ser regado con 100ml de agua.");
            rdy1.setText("Su cultivo debe ser puesto al sol durante 2 horas.");

        }else if(IdNoti== 002){
            rdy.setText("Su cultivo debe regarse con 30ml de agua");
            rdy1.setText("Su cultivo debe ser puesto al sol durante 2 horas.");
        }else if(IdNoti == 003){
            rdy.setText("Su cultivo debe ser regado con 100ml de agua.");
            rdy1.setText("Su cultivo debe ser puesto al sol durante 2 horas.");
        }

        nombre = (TextView) findViewById(R.id.nombreCultiv);
        fechaI = (TextView) findViewById(R.id.dateDura);
        p1 = (TextView) findViewById(R.id.pa1);
        p2 = (TextView) findViewById(R.id.pa2);
        p3 = (TextView) findViewById(R.id.pa3);
        p4 = (TextView) findViewById(R.id.pa4);
        p5 = (TextView) findViewById(R.id.pa5);
        p6 = (TextView) findViewById(R.id.pa6);
        p7 = (TextView) findViewById(R.id.pa7);
        p8 = (TextView) findViewById(R.id.pa8);
        p9 = (TextView) findViewById(R.id.pa9);
        consultarCultivo("http:/192.168.50.2/consultarpasosgermi.php?nombreCult=" + name);
        comparar();
    }
    private void consultarCultivo(String URL) {
        Log.i("url", "" + URL);
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    ja = new JSONArray(response);
                    nombreCulti = ja.getString(0);
                    fechaIn = ja.getString(1);
                    fechaFn = ja.getString(2);
                    paso1 = ja.getString(3);
                    paso2 = ja.getString(4);
                    paso3 = ja.getString(5);
                    paso4 = ja.getString(6);
                    paso5 = ja.getString(7);
                    paso6 = ja.getString(8);
                    paso7 = ja.getString(9);
                    paso8 = ja.getString(10);
                    paso9 = ja.getString(11);


                    if (nombreCulti.equals(name)) {
                        nombre.setText(nombreCulti);
                        if (!paso1.equals("null")) {
                            p1.setText(paso1);
                        }
                        if (!paso2.equals("null")) {
                            p2.setText(paso2);
                        }
                        if (!paso3.equals("null")) {
                            p3.setText(paso3);
                        }
                        if (!paso4.equals("null")) {
                            p4.setText(paso4);
                        }
                        if (!paso5.equals("null")) {
                            p5.setText(paso5);
                        }
                        if (!paso6.equals("null")) {
                            p6.setText(paso6);
                        }
                        if (!paso7.equals("null")) {
                            p7.setText(paso7);
                        }
                        if (!paso8.equals("null")) {
                            p8.setText(paso8);
                        }
                        if (!paso9.equals("null")) {
                            p9.setText(paso9);
                        }

                      /* SimpleDateFormat simpleDateFormat = new SimpleDateFormat("AAAA-MM-DD");
                        Date FfechaIn = simpleDateFormat.parse(fechaIn);
                        Date FfechaFn = simpleDateFormat.parse(fechaFn);
                        fechaI.setText(getDiferencia(FfechaIn, FfechaFn));*/

                    } else {
                        Toast.makeText(getApplicationContext(), "No se pudo agregar", Toast.LENGTH_LONG).show();

                    }


                } catch (JSONException e) {

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(stringRequest);
    }

    String dafe;

    public String getDiferencia(Date fechaInicial, Date fechaFinal) {

        long diferencia = fechaFinal.getTime() - fechaInicial.getTime();

        long segsMilli = 1000;
        long minsMilli = segsMilli * 60;
        long horasMilli = minsMilli * 60;
        long diasMilli = horasMilli * 24;
        long mesesMilli = diasMilli * 30;

        long mesesTranscurridos = diferencia / mesesMilli;
        diferencia = diferencia % mesesMilli;

        long diasTranscurridos = diferencia / diasMilli;
        diferencia = diferencia % diasMilli;

        long horasTranscurridos = diferencia / horasMilli;
        diferencia = diferencia % horasMilli;

        long minutosTranscurridos = diferencia / minsMilli;
        diferencia = diferencia % minsMilli;

        long segsTranscurridos = diferencia / segsMilli;



        return  "Duracion = "+ mesesTranscurridos;
        /*return "diasTranscurridos: " + diasTranscurridos + " , horasTranscurridos: " + horasTranscurridos +
                " , minutosTranscurridos: " + minutosTranscurridos + " , segsTranscurridos: " + segsTranscurridos;*/
    }
    public void comparar(){
        String url = "http:/192.168.50.2/BDremota/wsJSONConsultarUsuarioUrl.php?nombre="+name;

        jsonObjectRequest1 = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Cultivo cultivo = new Cultivo();

                JSONArray json = response.optJSONArray("cultivo");
                JSONObject jsonObject = null;

                try{

                    jsonObject = json.getJSONObject(0);

                    cultivo.setRutaImg(jsonObject.optString("ruta_imagenGe"));


                } catch (JSONException e) {

                    e.printStackTrace();
                }

                String UrlImg = cultivo.getRutaImg();
                cargarImg(UrlImg);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(jsonObjectRequest1);
    }

    private void cargarImg(String UrlImg) {
        UrlImg = UrlImg.replace("localhost","192.168.50.2");

        ImageRequest imageRequest = new ImageRequest(UrlImg, new Response.Listener<Bitmap>() {
            @Override
            public void onResponse(Bitmap response) {
                imgGe.setImageBitmap(response);
            }
        }, 0, 0, ImageView.ScaleType.CENTER, null, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(imageRequest);
    }


}
