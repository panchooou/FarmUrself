package com.example.franciscojavier.farmurself;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.franciscojavier.farmurself.Adaptadores.CultivosAdapter;
import com.example.franciscojavier.farmurself.Clases.Cultivo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import in.srain.cube.views.GridViewWithHeaderAndFooter;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class FragmentMainMenu extends Fragment implements Response.ErrorListener, Response.Listener<JSONObject> {

    private String addculti;
    private String D;
    View view1;
    JSONArray ja;
    RequestQueue requestQueue;
    ProgressDialog dialog;

    TextView culti, fechaI;
    //Se obtiene de Contenedor Fragment
    ImageView imagen;

    RecyclerView rvC;
    ArrayList<Cultivo> listaCultivos;
    ArrayList<Cultivo> Agre;
    JsonObjectRequest jsonObjectRequest;
    String wea = "";
    public String NombreCultivo1;
    public String NombreCultivo2;
    public String NombreCultivo3;
    public String NombreCultivo4;
    public String NombreCultivo5;
    public String NombreCultivo6;
    public String NombreCultivo7;
    public String NombreCultivo8;
    public String NombreCultivo9;
    public String NombreCultivo10;
    public String NombreCultivo11;
    public String NombreCultivo12;
    public String NombreCultivo13;
    public String NombreCultivo14;
    public String NombreCultivo15;
    public String NombreCultivo16;
    public String NombreCultivo17;
    public String NombreCultivo18;
    public String NombreCultivo19;
    public String NombreCultivo20;

    public String NombreCult;
    StringRequest stringRequest;

    public ZXingScannerView mScanner;
    // Argumento que muestra el numero de seccion al que pertenece
    private static final String ARG_SECTION_NUMBER = "section_number";
    // Se define el numero de seccion
    public static FragmentMainMenu newInstance(int sectionNumber){
        FragmentMainMenu fragment = new FragmentMainMenu();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }
    public FragmentMainMenu(){
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Bundle parametro = new Bundle();
        //parametro.putString("NameKey2", NombreCultivo);

        requestQueue = Volley.newRequestQueue(getContext());

        /*if (pasar = true){

            Fragment frag1 = null;
            frag1 = new ContenedorFragment();
            final FragmentTransaction ft1 = getFragmentManager().beginTransaction();
            ft1.replace(R.id.coordinator, frag1).commit();
            pasar = false;

        }*/


        /*Fragment FragmentMainMenu = new Fragment();
        FragmentMainMenu.setArguments(parametro);
        final FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.coordinator, FragmentMainMenu).commit();*/

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View rootView = inflater.inflate(R.layout.lista_cultivos, container, false);

        //Toast.makeText(getActivity(), NombreCultivo, Toast.LENGTH_SHORT).show();
        rvC = (RecyclerView) rootView.findViewById(R.id.rvId);
        rvC.setHasFixedSize(true);
        listaCultivos = new ArrayList<>();

        Agre = new ArrayList<>();

        rvC.setLayoutManager(new LinearLayoutManager(this.getContext()));

        requestQueue = Volley.newRequestQueue(getContext());



        if (!listaCultivos.isEmpty()){


            Fragment frag = null;
            frag = new ContenedorFragment();

            final FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.coordinator, frag).commit();

        }

        NombreCult = getArguments().getString("Nombree");

        if (NombreCult != null){



            Bundle bundle2 = new Bundle();
            bundle2.putString("Nombree1", NombreCult);
           // Toast.makeText(getActivity(), NombreCultivo, Toast.LENGTH_SHORT).show();
            Fragment frag = null;
            frag = new ContenedorFragment();
            frag.setArguments(bundle2);
            final FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.coordinator, frag).commit();


        }


        GridViewWithHeaderAndFooter grid = (GridViewWithHeaderAndFooter) rootView.findViewById(R.id.gridview);


        //Se inicializa el grid view
        InstanciarGV(grid);

        return rootView;
    }





    //metodo para instanciar el grid view dependiendo del numero de seccion al que pertenece
    private void InstanciarGV(GridViewWithHeaderAndFooter grid){
        int section_number = getArguments().getInt(ARG_SECTION_NUMBER);
        switch (section_number) {
            case 1:

                //Header

                //grid.addHeaderView(crearHeader(6, ListCultivos.getPlanta()));
                //grid.setAdapter(new GridAdapter(getActivity(), ListCultivos.getPlanta()));

                CultivosAdapter adapter1 = new CultivosAdapter(Agre);
                rvC.setAdapter(adapter1);


                break;
            case 2:
                //Aqui debe ir el fragment que escanea codigos QR
                //grid.setAdapter(new GridAdapter(getActivity(), ListCultivos.getPlanta()));
                //CultivosAdapter adapter = new CultivosAdapter(listaCultivos);
                //rvC.setAdapter(adapter);
                consultarCultivo();


                break;
        }
    }

    //Metodo que crea un grid de cabecera antes del grid view
    public void LlenarLista(){




    }
   private View EscanerView(Class<Escaner> V){
    View view;
       LayoutInflater inflater = getActivity().getLayoutInflater();
       view = inflater.inflate(R.layout.activity_qr, null, false);

        return view;

    }

    private void consultarCultivo() {
        dialog = new ProgressDialog(getContext());
        dialog.setMessage("Consultando Imagenes...");
        dialog.show();
        String url = "http://192.168.50.2/BDremota/wsJSONConsultarListaImagenesUrl.php";
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        requestQueue.add(jsonObjectRequest);

    }



    @Override
    public void onResponse(JSONObject response) {
        Cultivo cultivo = null;
        Cultivo add = null;

        JSONArray json = response.optJSONArray("nombre");

        try{
            for (int i=0;i<json.length();i++){
                cultivo= new Cultivo();
                JSONObject jsonObject = null;
                jsonObject = json.getJSONObject(i);

                cultivo.setNombre(jsonObject.optString("nombre"));
                cultivo.setDato(jsonObject.optString("imagen"));
                if (jsonObject.optString("nombre").equals("Apio")){
                    cultivo.setDuracion("5 Meses");
                }else if (jsonObject.optString("nombre").equals("Ciboulette")){
                    cultivo.setDuracion("1 Meses");
                }else if (jsonObject.optString("nombre").equals("Albahaca")){
                    cultivo.setDuracion("3 Meses");
                }
                listaCultivos.add(cultivo);


                    /*NombreCultivo1 = listaCultivos.get(0).getNombre();
                    NombreCultivo2 = listaCultivos.get(1).getNombre();
                    NombreCultivo3 = listaCultivos.get(2).getNombre();
                    NombreCultivo4 = listaCultivos.get(3).getNombre();
                    NombreCultivo5 = listaCultivos.get(4).getNombre();
                    NombreCultivo6 = listaCultivos.get(5).getNombre();
                    NombreCultivo7 = listaCultivos.get(6).getNombre();
                    NombreCultivo8 = listaCultivos.get(7).getNombre();
                    NombreCultivo9 = listaCultivos.get(8).getNombre();
                    NombreCultivo10 = listaCultivos.get(9).getNombre();
                    NombreCultivo11 = listaCultivos.get(10).getNombre();
                    NombreCultivo12 = listaCultivos.get(11).getNombre();
                    NombreCultivo13 = listaCultivos.get(12).getNombre();
                    NombreCultivo14 = listaCultivos.get(13).getNombre();
                    NombreCultivo15 = listaCultivos.get(14).getNombre();
                    NombreCultivo16 = listaCultivos.get(15).getNombre();
                    NombreCultivo17 = listaCultivos.get(16).getNombre();
                    NombreCultivo18 = listaCultivos.get(17).getNombre();
                    NombreCultivo19 = listaCultivos.get(18).getNombre();
                    NombreCultivo20 = listaCultivos.get(19).getNombre();*/



               // Toast.makeText(getContext(), NombreCultivo1, Toast.LENGTH_SHORT).show();
            }
            dialog.hide();

            CultivosAdapter adapter = new CultivosAdapter(listaCultivos);
            rvC.setAdapter(adapter);

        } catch (JSONException e) {
            dialog.hide();
            e.printStackTrace();
        }
    }



    @Override
    public void onErrorResponse(VolleyError error) {

    }



    private void cargarImagen(){
        String imgURL = "http://192.168.50.2/BDremota/imagenes/alv.jpg";

        ImageRequest imageRequest = new ImageRequest(imgURL, new Response.Listener<Bitmap>() {
            @Override
            public void onResponse(Bitmap response) {
                imagen.setImageBitmap(response);
            }
        }, 0,0, ImageView.ScaleType.CENTER, null, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(imageRequest);
    }


}
