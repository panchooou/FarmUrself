package com.example.franciscojavier.farmurself.Adaptadores;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.franciscojavier.farmurself.Clases.Cultivo;
import com.example.franciscojavier.farmurself.R;

import java.util.ArrayList;

public class CultivosAdapter extends RecyclerView.Adapter<CultivosAdapter.ViewHolderDatos> {
    ArrayList<Cultivo> listCultivos;

    public CultivosAdapter(ArrayList<Cultivo> listCultivos) {
        this.listCultivos = listCultivos;
    }





    @Override
    public ViewHolderDatos onCreateViewHolder( ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_item,parent,false);

        return new ViewHolderDatos(view);
    }

    @Override
    public void onBindViewHolder(ViewHolderDatos holder, int position) {
        holder.nombre.setText(listCultivos.get(position).getNombre());
        holder.duracion.setText(listCultivos.get(position).getDuracion());


        if (listCultivos.get(position).getImage()!=null){
            holder.image.setImageBitmap(listCultivos.get(position).getImage());
        }else {
            holder.image.setImageResource(R.drawable.ic_camera_alt_black_24dp);
        }
    }

    @Override
    public int getItemCount() {
        return listCultivos.size();
    }

    public class ViewHolderDatos extends RecyclerView.ViewHolder {
         TextView nombre,duracion;
         ImageView image;


        public ViewHolderDatos(View itemView) {
            super(itemView);
            nombre= (TextView) itemView.findViewById(R.id.nombre);
            duracion=(TextView) itemView.findViewById(R.id.duracion);
            image=(ImageView) itemView.findViewById(R.id.imagen);

        }


    }


}
