package com.example.franciscojavier.farmurself.InfoCultivos;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.Time;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.franciscojavier.farmurself.ActivityUsuario;
import com.example.franciscojavier.farmurself.Adaptadores.ExpandableListAdapter;
import com.example.franciscojavier.farmurself.Clases.Cultivo;
import com.example.franciscojavier.farmurself.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class InfoAlba extends AppCompatActivity implements  ListView.OnItemClickListener {
    TextView txtName, txtFecha;
    Button btnFecha;
    String dato = "Albahaca";
    RequestQueue request;
    RequestQueue requestQueue;
    JsonObjectRequest jsonObjectRequest, jsonObjectRequest1;
    ImageView CImg;
    String date, Hoy, Fin;
    JSONArray ja;
    NotificationCompat.Builder Noti;

    int NotificacionParaRegarEn30 = 003;

    private ExpandableListView listView;
    private ExpandableListAdapter listAdapter;
    private List<String> listData;
    private HashMap<String,List<String>> listHashMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_alba);

        request = Volley.newRequestQueue(getApplicationContext());
        requestQueue = Volley.newRequestQueue(getApplicationContext());
        listView = (ExpandableListView)findViewById(R.id.lvExp);
        initData();
        listAdapter = new ExpandableListAdapter(this, listData,listHashMap);
        listView.setAdapter(listAdapter);

        CImg = (ImageView)findViewById(R.id.CImge2);

        txtName = (TextView)findViewById(R.id.name_cultivo2);
        /*txtGerminacion = (TextView)findViewById(R.id.germinacion_cult);
        txtCrecimiento = (TextView) findViewById(R.id.crecimiento_cult);
        txtCosecha = (TextView)findViewById(R.id.cosecha_cult);
        txtInfo = (TextView)findViewById(R.id.info_cult); */

        //con esto obtiene desde el Drawer Activity, lo comento para testear.
        //Bundle bolsa1 = getIntent().getExtras();
        //txtName.setText(bolsa1.getString("NameKey"));

        final Bundle bundle = new Bundle();
        bundle.putString("Cult", dato);

        txtName.setText(dato);

        comparar();


        //Toast.makeText(this, Germina, Toast.LENGTH_SHORT).show();



        /*txtGerminacion.setText(bolsa1.getString("GermiKey"));
        txtCrecimiento.setText(bolsa1.getString("CreciKey"));
        txtCosecha.setText(bolsa1.getString("CoseKey"));
        txtInfo.setText(bolsa1.getString("InfoKey"));*/

        llamarfecha();


        btnFecha = (Button)findViewById(R.id.btnAddFecha2);
        btnFecha.setEnabled(true);
        txtFecha = (TextView)findViewById(R.id.tvfecha2);

        Time hoy = new Time(Time.getCurrentTimezone());
        hoy.setToNow();
        Time mañana = new Time(Time.getCurrentTimezone());

        int mes1 = mañana.month + 5;

        int dia = hoy.monthDay;
        int mes = hoy.month;
        int ano = hoy.year;
        mes = mes +1;
        Hoy = (ano+"-"+mes+"-"+dia);
        Fin = (ano+"-"+mes1+"-"+dia);

        btnFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnFecha.setEnabled(false);

                modificarFecha();


            }
        });

        notificar();

        listView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                Intent intent  = null;
                switch (groupPosition){
                    case 0:
                        switch (childPosition){
                            case 0:
                                intent  = new Intent(InfoAlba.this, InfoGermi.class);
                                intent.putExtras(bundle);
                                startActivity(intent);
                                break;
                        }
                        break;
                    case 1:
                        switch (childPosition){
                            case 0:
                                intent  = new Intent(InfoAlba.this, InfoCreci.class);
                                intent.putExtras(bundle);
                                startActivity(intent);
                                break;
                        }
                        break;
                    case 2:
                        switch (childPosition){
                            case 0:
                                intent  = new Intent(InfoAlba.this, InfoCose.class);
                                intent.putExtras(bundle);
                                startActivity(intent);
                                break;
                        }
                        break;
                }
                return false;
            }
        });

    }

    private void initData() {
        listData = new ArrayList<>();
        listHashMap = new HashMap<>();

        listData.add("Germinacion");
        listData.add("Crecimiento");
        listData.add("Cosecha");

        List<String> Germinacion = new ArrayList<>();
        Germinacion.add("Proceso en el cual se prepara la planta para que salga de sus semillas. Seleccione para más detalles");

        List<String> Crecimiento = new ArrayList<>();
        Crecimiento.add("Proceso en el cual se planta el cultivo y se le aplican sus respectivos cuidados para que crezca sana y segura. Seleccione para más detalles");

        List<String> Cosecha = new ArrayList<>();
        Cosecha.add("Proceso en el cual la planta está lista y madura. Seleccione para más detalles");

        listHashMap.put(listData.get(0),Germinacion);
        listHashMap.put(listData.get(1),Crecimiento);
        listHashMap.put(listData.get(2),Cosecha);




    }


    private void llamarfecha(){
        String url = "http://192.168.50.2/consultarfechaactual.php";
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    ja = new JSONArray(response);
                    date = ja.getString(0);

                    txtFecha.setText(date);

                } catch (Exception e){

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(stringRequest);
    }

    private void modificarFecha() {
        //Mensaje de cargando
        //Url a la cual se realizará la consulta
        String url = "http://192.168.50.2/modificarfecha.php?fecha_ini="+Hoy+"&fecha_fin="+Fin+"&nombreCult="+txtName.getText().toString();

        //Se realiza el llamado a la url e intenta conectarse.
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Toast.makeText(getApplicationContext(), "No se ha podido ingresar la fecha",Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Se ha ingresado la fecha actual.",Toast.LENGTH_SHORT).show();
            }
        });
        request.add(jsonObjectRequest);

    }


    public void notificar(){
        Noti = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_notifications_black_24dp)
                .setContentTitle("Farm Urself")
                .setContentText("Tu cultivo de "+txtName.getText().toString()+" necesita ser regado dentro de 30 minutos");

        Intent resultIntent = new Intent(this, ActivityUsuario.class);
        Bundle b = new Bundle();
        b.putString("KY", txtName.getText().toString());
        b.putInt("ID", NotificacionParaRegarEn30);
        resultIntent.putExtras(b);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(
                this,
                0,
                resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT
        );
        Noti.setContentIntent(resultPendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(NotificacionParaRegarEn30, Noti.build());
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    public void comparar(){
        String url = "http:/192.168.50.2/BDremota/wsJSONConsultarUsuarioUrl.php?nombre="+txtName.getText().toString();

        jsonObjectRequest1 = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Cultivo cultivo = new Cultivo();

                JSONArray json = response.optJSONArray("cultivo");
                JSONObject jsonObject = null;

                try{

                    jsonObject = json.getJSONObject(0);

                    cultivo.setRutaImg(jsonObject.optString("ruta_imagen"));


                } catch (JSONException e) {

                    e.printStackTrace();
                }

                String UrlImg = cultivo.getRutaImg();
                cargarImg(UrlImg);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(jsonObjectRequest1);
    }

    private void cargarImg(String UrlImg) {
        UrlImg = UrlImg.replace("localhost","192.168.50.2");

        ImageRequest imageRequest = new ImageRequest(UrlImg, new Response.Listener<Bitmap>() {
            @Override
            public void onResponse(Bitmap response) {
                CImg.setImageBitmap(response);
            }
        }, 0, 0, ImageView.ScaleType.CENTER, null, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        request.add(imageRequest);
    }

}
