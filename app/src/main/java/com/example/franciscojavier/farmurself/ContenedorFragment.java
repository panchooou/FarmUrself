package com.example.franciscojavier.farmurself;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.franciscojavier.farmurself.Adaptadores.SeccionesAdapter;
import com.example.franciscojavier.farmurself.Clases.Utilidades;



public class ContenedorFragment extends Fragment {


    private OnFragmentInteractionListener mListener;
    View vista;
    private AppBarLayout appBar;
    private TabLayout pestañas;
    private ViewPager viewPager;
    String myValue;
    private String nameee;

    public ContenedorFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // recibe desde el Drawer Activity

        //Toast.makeText(getContext(), myValue, Toast.LENGTH_SHORT).show();

        // Envia a Fragment Main menu
        /*Bundle parametro = new Bundle();
        parametro.putString("NameKey2", myValue);*/

        /*Fragment FragmentMainMenu = new Fragment();
        FragmentMainMenu.setArguments(parametro);
        final FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.coordinator, FragmentMainMenu).commit();*/
        if (getArguments() != null){
            nameee = getArguments().getString("Nombree1");
           //Toast.makeText(getActivity(), nameee, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        vista = inflater.inflate(R.layout.fragment_contenedor, container, false);


        if (Utilidades.rotacion==0){ //Si no hay pestañas creadas, procederá a crear una.

            View parent = (View) container.getParent();

            if (appBar == null){
                appBar= (AppBarLayout) parent.findViewById(R.id.appbar);
                pestañas= new TabLayout(getActivity());
                appBar.addView(pestañas);

                viewPager = (ViewPager) vista.findViewById(R.id.idViewPager);
                llenarViewPager(viewPager);
                viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener(){
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                        super.onPageScrolled(position, positionOffset, positionOffsetPixels);
                    }
                });
                pestañas.setupWithViewPager(viewPager);
            }
            pestañas.setTabGravity(TabLayout.GRAVITY_FILL);
        }else { // Al haber 1 pestaña en la variable no se vuelve a instanciar las pestañas.
            Utilidades.rotacion = 1;
        }


        return vista;
    }


    private void llenarViewPager(ViewPager viewPager) {
        SeccionesAdapter adapter = new SeccionesAdapter(getFragmentManager());
        adapter.addFragment(FragmentMainMenu.newInstance(1), "Mis Cultivos");
        adapter.addFragment(FragmentMainMenu.newInstance(2), "Listado de cultivos");

        viewPager.setAdapter(adapter);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(Utilidades.rotacion==0){
            appBar.removeView(pestañas);
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


}
