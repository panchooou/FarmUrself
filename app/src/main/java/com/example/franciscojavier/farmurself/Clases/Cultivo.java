package com.example.franciscojavier.farmurself.Clases;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

public class Cultivo {
    //POJO de los cultivos

    //nombre del cultivo
    private String nombre;

    //detalle del cultivo
    private String duracion;

    private String dato;

    //identificador de imagen de cultivo
    private Bitmap Image;
    private String rutaImg;


    public Cultivo(String nombre, String duracion, Bitmap Image) {
        this.nombre = nombre;

        this.Image = Image;
    }
    public  Cultivo(){
    }

    public String getDato() {
        return dato;
    }

    public void setDato(String dato) {
        this.dato = dato;
        try {
            byte[] byteCode= Base64.decode(dato,Base64.DEFAULT);
            this.Image = BitmapFactory.decodeByteArray(byteCode,0,byteCode.length);

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public String getRutaImg() {
        return rutaImg;
    }

    public void setRutaImg(String rutaImg) {
        this.rutaImg = rutaImg;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDuracion() {
        return duracion;
    }

    public Bitmap getImage() {
        return Image;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }

    public void setImage(Bitmap Image) {
        this.Image = Image;
    }
}
