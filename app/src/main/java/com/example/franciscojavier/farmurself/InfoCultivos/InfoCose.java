package com.example.franciscojavier.farmurself.InfoCultivos;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.franciscojavier.farmurself.Clases.Cultivo;
import com.example.franciscojavier.farmurself.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

public class InfoCose extends AppCompatActivity {
    JSONArray ja;
    String name;
    TextView p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18;
    TextView fechaI, fechaF, nombre;
    ImageView imgCo;
    JsonObjectRequest jsonObjectRequest1;
    RequestQueue requestQueue;
    String paso1, paso2, paso3, paso4, paso5, paso6, paso7, paso8, paso9, paso10, paso11, paso12, paso13, paso14, paso15, paso16, paso17, paso18;
    String fechaIn, fechaFn, nombreCulti, fi, fn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_cose);

        requestQueue = Volley.newRequestQueue(getApplicationContext());

        Bundle obtiene = this.getIntent().getExtras();
        name = obtiene.getString("Cult");

        imgCo = (ImageView) findViewById(R.id.imgCose);

        nombre = (TextView) findViewById(R.id.nombreCultivo);
        fechaI = (TextView) findViewById(R.id.dateDura);
        p1 = (TextView) findViewById(R.id.paso1);
        p2 = (TextView) findViewById(R.id.paso2);
        p3 = (TextView) findViewById(R.id.paso3);
        p4 = (TextView) findViewById(R.id.paso4);
        p5 = (TextView) findViewById(R.id.paso5);
        p6 = (TextView) findViewById(R.id.paso6);
        p7 = (TextView) findViewById(R.id.paso7);
        p8 = (TextView) findViewById(R.id.paso8);
        p9 = (TextView) findViewById(R.id.paso9);
        p10 = (TextView) findViewById(R.id.paso10);
        p11 = (TextView) findViewById(R.id.paso11);
        p12 = (TextView) findViewById(R.id.paso12);
        p13 = (TextView) findViewById(R.id.paso13);
        p14 = (TextView) findViewById(R.id.paso14);
        p15 = (TextView) findViewById(R.id.paso15);
        p16 = (TextView) findViewById(R.id.paso16);
        p17 = (TextView) findViewById(R.id.paso17);
        p18 = (TextView) findViewById(R.id.paso18);



        consultarCultivo("http:/192.168.50.2/consultarpasoscose.php?nombreCult=" + name);
        comparar();

    }

    private void consultarCultivo(String URL) {
        Log.i("url", "" + URL);
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    ja = new JSONArray(response);
                    nombreCulti = ja.getString(0);
                    fechaIn = ja.getString(1);
                    fechaFn = ja.getString(2);
                    paso1 = ja.getString(3);
                    paso2 = ja.getString(4);
                    paso3 = ja.getString(5);
                    paso4 = ja.getString(6);
                    paso5 = ja.getString(7);
                    paso6 = ja.getString(8);
                    paso7 = ja.getString(9);
                    paso8 = ja.getString(10);
                    paso9 = ja.getString(11);
                    paso10 = ja.getString(12);
                    paso11 = ja.getString(13);
                    paso12 = ja.getString(14);
                    paso13 = ja.getString(15);
                    paso14 = ja.getString(16);
                    paso15 = ja.getString(17);
                    paso16 = ja.getString(18);
                    paso17 = ja.getString(19);
                    paso18 = ja.getString(20);

                    if (nombreCulti.equals(name)) {
                        nombre.setText(nombreCulti);
                        if (!paso1.equals("null")) {
                            p1.setText(paso1);
                        }
                        if (!paso2.equals("null")) {
                            p2.setText(paso2);
                        }
                        if (!paso3.equals("null")) {
                            p3.setText(paso3);
                        }
                        if (!paso4.equals("null")) {
                            p4.setText(paso4);
                        }
                        if (!paso5.equals("null")) {
                            p5.setText(paso5);
                        }
                        if (!paso6.equals("null")) {
                            p6.setText(paso6);
                        }
                        if (!paso7.equals("null")) {
                            p7.setText(paso7);
                        }
                        if (!paso8.equals("null")) {
                            p8.setText(paso8);
                        }
                        if (!paso9.equals("null")) {
                            p9.setText(paso9);
                        }
                        if (!paso10.equals("null")) {
                            p10.setText(paso10);
                        }
                        if (!paso11.equals("null")) {
                            p11.setText(paso11);
                        }
                        if (!paso12.equals("null")) {
                            p12.setText(paso12);
                        }
                        if (!paso13.equals("null")) {
                            p13.setText(paso13);
                        }
                        if (!paso14.equals("null")) {
                            p14.setText(paso14);
                        }
                        if (!paso15.equals("null")) {
                            p15.setText(paso15);
                        }
                        if (!paso16.equals("null")) {
                            p16.setText(paso16);
                        }
                        if (!paso17.equals("null")) {
                            p17.setText(paso17);
                        }
                        if (!paso18.equals("null")) {
                            p18.setText(paso18);
                        }

                        /*SimpleDateFormat simpleDateFormat = new SimpleDateFormat("AAAA-MM-DD");
                        Date FfechaIn = simpleDateFormat.parse(fechaIn);
                        Date FfechaFn = simpleDateFormat.parse(fechaFn);
                        fechaI.setText(getDiferencia(FfechaIn, FfechaFn));*/

                    } else {
                        Toast.makeText(getApplicationContext(), "No se pudo agregar", Toast.LENGTH_LONG).show();

                    }


                } catch (JSONException e) {

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(stringRequest);
    }

    String dafe;

    public String getDiferencia(Date fechaInicial, Date fechaFinal) {

        long diferencia = fechaFinal.getTime() - fechaInicial.getTime();

        long segsMilli = 1000;
        long minsMilli = segsMilli * 60;
        long horasMilli = minsMilli * 60;
        long diasMilli = horasMilli * 24;
        long mesesMilli = diasMilli * 30;

        long mesesTranscurridos = diferencia / mesesMilli;
        diferencia = diferencia % mesesMilli;

        long diasTranscurridos = diferencia / diasMilli;
        diferencia = diferencia % diasMilli;

        long horasTranscurridos = diferencia / horasMilli;
        diferencia = diferencia % horasMilli;

        long minutosTranscurridos = diferencia / minsMilli;
        diferencia = diferencia % minsMilli;

        long segsTranscurridos = diferencia / segsMilli;



        return  "Duracion = "+ mesesTranscurridos;
        /*return "diasTranscurridos: " + diasTranscurridos + " , horasTranscurridos: " + horasTranscurridos +
                " , minutosTranscurridos: " + minutosTranscurridos + " , segsTranscurridos: " + segsTranscurridos;*/
    }
    public void comparar(){
        String url = "http:/192.168.50.2/BDremota/wsJSONConsultarUsuarioUrl.php?nombre="+name;

        jsonObjectRequest1 = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Cultivo cultivo = new Cultivo();

                JSONArray json = response.optJSONArray("cultivo");
                JSONObject jsonObject = null;

                try{

                    jsonObject = json.getJSONObject(0);

                    cultivo.setRutaImg(jsonObject.optString("ruta_imagenCo"));


                } catch (JSONException e) {

                    e.printStackTrace();
                }

                String UrlImg = cultivo.getRutaImg();
                cargarImg(UrlImg);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(jsonObjectRequest1);
    }

    private void cargarImg(String UrlImg) {
        UrlImg = UrlImg.replace("localhost","192.168.50.2");

        ImageRequest imageRequest = new ImageRequest(UrlImg, new Response.Listener<Bitmap>() {
            @Override
            public void onResponse(Bitmap response) {
                imgCo.setImageBitmap(response);
            }
        }, 0, 0, ImageView.ScaleType.CENTER, null, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(imageRequest);
    }
}
