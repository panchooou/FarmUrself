package com.example.franciscojavier.farmurself;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.zxing.Result;

import org.json.JSONArray;
import org.json.JSONException;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class Escaner extends AppCompatActivity implements ZXingScannerView.ResultHandler{

    public static String name;
    boolean ifvacio = false;
    public static JSONArray ja;
    public static String Germi;
    public static String Cose;
    public static String info;
    public static String Creci;
    Bundle bolsa1 = new Bundle();
    boolean abier = false;




    private ZXingScannerView mScanner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr);








    }

    public void Escaner(View view){
        mScanner = new ZXingScannerView(this);
        setContentView(mScanner);
        abier = true;
        mScanner.setResultHandler(this);
        mScanner.startCamera();


    }


    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    public void onBackPressed() {

        Intent intent1 = new Intent(Escaner.this, DrawerActivity.class);
        startActivity(intent1);
        finish();
        super.onBackPressed();


    }

    @Override
    public void handleResult(Result result) {
        Log.v("HandleResult",result.getText());
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Resultado del Escaner");
        builder.setMessage(result.getText());
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

        name = result.getText();
        mScanner.stopCamera();
        consultarCultivo("http://192.168.50.2/consultarcultivo.php?nombre_cultivo=" + name);



    }

private void pasar(){
    Intent intent1 = new Intent(Escaner.this, DrawerActivity.class);
    bolsa1.putString("NameKey", name);

    intent1.putExtras(bolsa1);
    startActivity(intent1);
    finish();
}

    private void consultarCultivo(String URL){
        Log.i("url",""+URL);
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    ja = new JSONArray(response);
                    String nombreCulti = ja.getString(0);
                    if (nombreCulti.equals(name)) {



                        Intent intent1 = new Intent(Escaner.this, DrawerActivity.class);
                        bolsa1.putString("NameKey", nombreCulti);
                        intent1.putExtras(bolsa1);
                        startActivity(intent1);
                        finish();
                        nombreCulti = null;
                       // consultarGermi("http://192.168.50.2/consultargermi.php?nombre_cultivo="+name);

                        //DRAMA



                    } else {
                        Toast.makeText(getApplicationContext(), "No se pudo agregar", Toast.LENGTH_LONG).show();
                    }


                } catch (JSONException e) {

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(stringRequest);
    }

        private void agregarCultivo(String URL){
            Log.i("url",""+URL);
            RequestQueue queue = Volley.newRequestQueue(this);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    response = response.replace("][",",");
                    if (response.length()>0){
                        try {
                            JSONArray je = new JSONArray(response);

                            Toast.makeText(getApplicationContext(),"Json="+je,Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            queue.add(stringRequest);
    }

    private void consultarGermi(String URL){
     Log.i("url",""+URL);
    RequestQueue queue = Volley.newRequestQueue(this);
    StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
        @Override
        public void onResponse(String response) {
            try {
                ja = new JSONArray(response);
                Germi = ja.getString(0);


                    consultarCreci("http://192.168.50.2/consultarcreci.php?nombre_cultivo="+name);



            } catch (JSONException e) {

            }
        }
    }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {

        }
    });
        queue.add(stringRequest);
}
    private void consultarCreci(String URL){
        Log.i("url",""+URL);
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    ja = new JSONArray(response);
                   Creci = ja.getString(0);



                    consultarCose("http://192.168.50.2/consultarcose.php?nombre_cultivo="+name);

                } catch (JSONException e) {

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(stringRequest);
    }
    private void consultarCose(String URL){
        Log.i("url",""+URL);
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    ja = new JSONArray(response);
                    Cose = ja.getString(0);


                   consultarInfo("http://192.168.50.2/consultarinfo.php?nombre_cultivo="+name);


                } catch (JSONException e) {

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(stringRequest);
    }
    private void consultarInfo(String URL){
        Log.i("url",""+URL);
        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    ja = new JSONArray(response);
                    info = ja.getString(0);

                   // Toast.makeText(Escaner.this,Germi+Creci+Cose+info, Toast.LENGTH_SHORT).show();
                    /*Intent intent1 = new Intent(Escaner.this, InfoCultActivity.class);
                    bolsa1.putString("NameKey", name);
                    bolsa1.putString("GermiKey", Germi);
                    bolsa1.putString("CreciKey", Creci);
                    bolsa1.putString("CoseKey", Cose);
                    intent1.putExtras(bolsa1);
                    startActivity(intent1);*/


                } catch (JSONException e) {

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(stringRequest);
    }

}
