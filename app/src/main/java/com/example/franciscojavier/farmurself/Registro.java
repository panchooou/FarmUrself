package com.example.franciscojavier.farmurself;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Registro extends AppCompatActivity implements Response.Listener<JSONObject>, Response.ErrorListener {

    private EditText mEdtEmail, User, email, pass, pass2;
    private Button mBtnRegistrar;
    ProgressDialog progreso;

    //Estas variable permiten la conexion.
    RequestQueue request;
    JsonObjectRequest jsonObjectRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);


        mEdtEmail = (EditText)findViewById(R.id.regisEmail);
        User = (EditText)findViewById(R.id.regisUsser);
        email = (EditText)findViewById(R.id.regisEmail);
        pass = (EditText)findViewById(R.id.regisPass);
        pass2 = (EditText)findViewById(R.id.regisPass2);
        mBtnRegistrar = (Button)findViewById(R.id.btn_RegistrarUsuario);

        User.requestFocus();

        request = Volley.newRequestQueue(getApplicationContext());
        mBtnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String verificarE = mEdtEmail.getText().toString();
                //dramon de aquellos, revisar


                if (condiciones(true)){
                    if (validarPass(true)) {
                    pass.requestFocus();
                        if (!validarEmail(verificarE)) {
                            email.requestFocus();
                            mEdtEmail.setError("Email invalido");
                }else conectarWeb();
                    }
                }
        }
        });


    }

    private void conectarWeb() {
        //Mensaje de cargando
        progreso = new ProgressDialog(this);
        progreso.setMessage("Cargando...");
        progreso.show();
        //Url a la cual se realizará la consulta
        String url = "http:/192.168.50.2/register.php?username="+User.getText().toString()+"&email="+email.getText().toString()
        +"&password="+ pass.getText().toString();

        //Se realiza el llamado a la url e intenta conectarse.
    jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
    request.add(jsonObjectRequest);
    }
    //Cuando el servidor responde
    @Override
    public void onResponse(JSONObject response) {
        progreso.hide();
        Toast.makeText(this, "Se ha producido un error al registrarse",Toast.LENGTH_SHORT).show();
    }

    //Cuando se emite un error al conectar con el servidor
    @Override
    public void onErrorResponse(VolleyError error) {
        progreso.hide();
        Toast.makeText(this, "Se ha registrado exitosamente",Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }



    public static boolean validarEmail(String email){

        String regex = "^[A-Za-z0-9+_.-]+@+[a-z]+.+[a-z]$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(email);

        return matcher.matches();
    }

    public boolean validarPass(boolean contra){
        String ver1 = pass.getText().toString();
        String ver2 = pass2.getText().toString();
        if (!ver1.equals(ver2)){
            contra = true;
            pass.setError("Las contraseñas no coinciden");
            pass2.setError("Las contraseñas no coinciden");
        }
        return contra;
    }

    public boolean condiciones(boolean cond){

        String usuario = User.getText().toString();
        String mail = email.getText().toString();
        String p1 = pass.getText().toString();
        String p2 = pass2.getText().toString();

        if (usuario.equals("")){
            User.setError("El campo está vacio");
            User.requestFocus();
            if (mail.equals("")){
                email.setError("El campo está vacio");
                email.requestFocus();
                if (p1.equals("")){
                    pass.setError("El campo está vacio");
                    pass.requestFocus();
                    if (p2.equals("")){
                        pass2.setError("El campo está vacio");
                        pass2.requestFocus();
                        cond = true;
                    }
                }
            }
        }

        return cond;
    }

}
