package com.example.franciscojavier.farmurself;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AgregarNuevoCultivo extends AppCompatActivity implements Response.ErrorListener, Response.Listener<JSONObject> {
    private static final int COD_SELECCIONA = 10;
    private static final int COD_SELECCIONA1 = 11;
    private static final int COD_SELECCIONA2 = 12;
    private static final int COD_SELECCIONA3 = 13;
    private LinearLayout mLayout,mLayout1,mLayout2;
    private EditText mEditText,mEditText1,mEditText2, nombreCultivo, fechaIni, fechaFin;
    private Button mButton, mButton1, mButton2, btnAgregar, mButton5, btn6, btn7, btn8;
    private ImageView foto, fotoGe, fotoCre, fotoCo;
    int pasoC = 0;
    int pasoG = 0;
    int pasoCo = 0;
    ProgressDialog progreso;
    RequestQueue request;
    JsonObjectRequest jsonObjectRequest;
    Bitmap bitmap, bitmap1, bitmap2, bitmap3;
    StringRequest stringRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_nuevo_cultivo);

        mButton5 = (Button)findViewById(R.id.button5);
        btn6 = (Button)findViewById(R.id.button6);
        btn7 = (Button)findViewById(R.id.button7);
        btn8 = (Button)findViewById(R.id.button8);
        foto = (ImageView)findViewById(R.id.imgFoto);
        fotoGe = (ImageView)findViewById(R.id.imgFotoGermi);
        fotoCre = (ImageView)findViewById(R.id.imgFotoCreci);
        fotoCo = (ImageView)findViewById(R.id.imgFotoCose);

        nombreCultivo = (EditText) findViewById(R.id.nombre_cult);
        fechaIni = (EditText)findViewById(R.id.dateIni);
        fechaFin = (EditText)findViewById(R.id.dateFin);

        btnAgregar = (Button)findViewById(R.id.btnAgregar);

        mLayout = (LinearLayout) findViewById(R.id.linearlayou);
        mEditText = (EditText) findViewById(R.id.editText1);
        mButton = (Button) findViewById(R.id.button1);

        mLayout1 = (LinearLayout) findViewById(R.id.linearlayou2);
        mEditText1 = (EditText) findViewById(R.id.editText2);
        mButton1 = (Button) findViewById(R.id.button2);

        mLayout2 = (LinearLayout) findViewById(R.id.linearlayou3);
        mEditText2 = (EditText) findViewById(R.id.editText3);
        mButton2 = (Button) findViewById(R.id.button3);

        request = Volley.newRequestQueue(getApplicationContext());

        mButton.setOnClickListener(onClick1());
        TextView textView = new TextView(this);
        textView.setText("New text");

        mButton1.setOnClickListener(onClick2());
        TextView textView1 = new TextView(this);
        textView1.setText("New text");

        mButton2.setOnClickListener(onClick3());
        TextView textView2 = new TextView(this);
        textView2.setText("New text");

        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cargarWeb();
                crearPasosGermi();
                crearPasosCreci();
                crearPasosCose();
                Intent intent = new Intent(AgregarNuevoCultivo.this, DrawerActivity.class);
                startActivity(intent);
                finish();
            }
        });

        mButton5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/");
                startActivityForResult(intent.createChooser(intent,"Seleccione"),COD_SELECCIONA);
            }
        });
        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/");
                startActivityForResult(intent.createChooser(intent,"Seleccione"),COD_SELECCIONA1);
            }
        });
        btn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/");
                startActivityForResult(intent.createChooser(intent,"Seleccione"),COD_SELECCIONA2);
            }
        });
        btn8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/");
                startActivityForResult(intent.createChooser(intent,"Seleccione"),COD_SELECCIONA3);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case COD_SELECCIONA:
                Uri miPath = data.getData();
                foto.setImageURI(miPath);

                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(),miPath);
                    foto.setImageBitmap(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                }

                break;
            case COD_SELECCIONA1:
                Uri miPath1 = data.getData();
                fotoGe.setImageURI(miPath1);

                try {
                    bitmap1 = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(),miPath1);
                    fotoGe.setImageBitmap(bitmap1);

                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case COD_SELECCIONA2:
                Uri miPath2 = data.getData();
                fotoCre.setImageURI(miPath2);

                try {
                    bitmap2 = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(),miPath2);
                    fotoCre.setImageBitmap(bitmap2);

                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case COD_SELECCIONA3:
                Uri miPath3 = data.getData();
                fotoCo.setImageURI(miPath3);

                try {
                    bitmap3 = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(),miPath3);
                    fotoCo.setImageBitmap(bitmap3);

                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }
    private void cargarWeb(){
        progreso = new ProgressDialog(this);
        progreso.setMessage("Cargando...");
        progreso.show();

        String url = "http:/192.168.50.2/BDremota/wsJSONRegistroMovil.php?";
        stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                String nombre = nombreCultivo.getText().toString();
                String nombreGe = nombreCultivo.getText().toString()+"Ge";
                String nombreCre = nombreCultivo.getText().toString()+"Cre";
                String nombreCo = nombreCultivo.getText().toString()+"Co";
                String imagen = convertImg(bitmap);
                String imagenGe = convertImg(bitmap1);
                String imagenCre = convertImg(bitmap2);
                String imagenCo = convertImg(bitmap3);
                Map<String, String> parametros = new HashMap<>();
                parametros.put("nombre", nombre);
                parametros.put("nombreGe", nombreGe);
                parametros.put("nombreCre", nombreCre);
                parametros.put("nombreCo", nombreCo);
                parametros.put("imagen", imagen);
                parametros.put("imagenGe", imagenGe);
                parametros.put("imagenCre", imagenCre);
                parametros.put("imagenCo", imagenCo);


                /*String datei = fechaIni.getText().toString();
                String datef = fechaFin.getText().toString();
                String p1 = pasos1[1];
                String p2 = pasos1[2];
                String p3 = pasos1[3];
                String p4 = pasos1[4];
                String p5 = pasos1[5];
                String p6 = pasos1[6];
                String p7 = pasos1[7];
                String p8 = pasos1[8];
                String p9 = pasos1[9];
                String p10 = pasos1[10];
                String p11 = pasos1[11];
                String p12 = pasos1[12];
                String p13 = pasos1[13];
                String p14 = pasos1[14];
                String p15 = pasos1[15];
                String p16 = pasos1[16];
                String p17 = pasos1[17];
                String p18 = pasos1[18];


                parametros.put("fechaIni", datei);
                parametros.put("fechaFin", datef);
                parametros.put("p1", p1);
                parametros.put("p2", p2);
                parametros.put("p3", p3);
                parametros.put("p4", p4);
                parametros.put("p5", p5);
                parametros.put("p6", p6);
                parametros.put("p7", p7);
                parametros.put("p8", p8);
                parametros.put("p9", p9);
                parametros.put("p10", p10);
                parametros.put("p11", p11);
                parametros.put("p12", p12);
                parametros.put("p13", p13);
                parametros.put("p14", p14);
                parametros.put("p15", p15);
                parametros.put("p16", p16);
                parametros.put("p17", p17);
                parametros.put("p18", p18);*/

                return parametros;

            }
        };
        request.add(stringRequest);
    }

    private String convertImg(Bitmap bitmap) {

        ByteArrayOutputStream array = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100, array);
        byte[] imagenByte = array.toByteArray();
        String imagenString = Base64.encodeToString(imagenByte,Base64.DEFAULT);

        return imagenString;
    }

    private void crearPasosGermi() {
        //Mensaje de cargando


        //Url a la cual se realizará la consulta
       String url = "http:/192.168.50.2/addpasosgermi.php?nombreCult="+nombreCultivo.getText().toString()+"&fecha_ini="+fechaIni.getText().toString()+"&fecha_fin="+fechaFin.getText().toString()+
               "&paso1="+pasos1[1]+"&paso2="+pasos1[2]+"&paso3="+pasos1[3]+"&paso4="+pasos1[4]+"&paso5="+pasos1[5]+"&paso6="+pasos1[6]+
               "&paso7="+pasos1[7]+"&paso8="+pasos1[8]+"&paso9="+pasos1[9]+"&paso10="+pasos1[10]+"&paso11="+pasos1[11]+"&paso12="+pasos1[12]+
               "&paso13="+pasos1[13]+"&paso14="+pasos1[14]+"&paso15="+pasos1[15]+"&paso16="+pasos1[16]+"&paso17="+pasos1[17]+"&paso18="+pasos1[18];



        //Se realiza el llamado a la url e intenta conectarse.
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
    }

    private void crearPasosCreci() {
        //Mensaje de cargando



        //Url a la cual se realizará la consulta
        String url = "http:/192.168.50.2/addpasoscreci.php?nombreCult="+nombreCultivo.getText().toString()+"&fecha_ini="+fechaIni.getText().toString()+"&fecha_fin="+fechaFin.getText().toString()+
                "&paso1="+pasos2[1]+"&paso2="+pasos2[2]+"&paso3="+pasos2[3]+"&paso4="+pasos2[4]+"&paso5="+pasos2[5]+"&paso6="+pasos2[6]+
                "&paso7="+pasos2[7]+"&paso8="+pasos2[8]+"&paso9="+pasos2[9]+"&paso10="+pasos2[10]+"&paso11="+pasos2[11]+"&paso12="+pasos2[12]+
                "&paso13="+pasos2[13]+"&paso14="+pasos2[14]+"&paso15="+pasos2[15]+"&paso16="+pasos2[16]+"&paso17="+pasos2[17]+"&paso18="+pasos2[18];



        //Se realiza el llamado a la url e intenta conectarse.
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
    }

    private void crearPasosCose() {
        //Mensaje de cargando

        //Url a la cual se realizará la consulta
        String url = "http:/192.168.50.2/addpasoscose.php?nombreCult="+nombreCultivo.getText().toString()+"&fecha_ini="+fechaIni.getText().toString()+"&fecha_fin="+fechaFin.getText().toString()+
                "&paso1="+pasos3[1]+"&paso2="+pasos3[2]+"&paso3="+pasos3[3]+"&paso4="+pasos3[4]+"&paso5="+pasos3[5]+"&paso6="+pasos3[6]+
                "&paso7="+pasos3[7]+"&paso8="+pasos3[8]+"&paso9="+pasos3[9]+"&paso10="+pasos3[10]+"&paso11="+pasos3[11]+"&paso12="+pasos3[12]+
                "&paso13="+pasos3[13]+"&paso14="+pasos3[14]+"&paso15="+pasos3[15]+"&paso16="+pasos3[16]+"&paso17="+pasos3[17]+"&paso18="+pasos3[18];

        //Se realiza el llamado a la url e intenta conectarse.
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
    }
    //Cuando el servidor responde
    @Override
    public void onResponse(JSONObject response) {
        progreso.hide();
        Toast.makeText(this, "Se ha producido un error al registrarse",Toast.LENGTH_SHORT).show();
    }

    //Cuando se emite un error al conectar con el servidor
    @Override
    public void onErrorResponse(VolleyError error) {
        progreso.hide();
        Toast.makeText(this, "Se ha registrado exitosamente",Toast.LENGTH_SHORT).show();

    }

    private View.OnClickListener onClick1() {
        return new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                pasoG++;
                paso = pasoG;
                mLayout.addView(createNewTextView(mEditText.getText().toString()));

                pasos1[pasoG] = add;
                mEditText.setText("");

            }
        };
    }
    private View.OnClickListener onClick2() {
        return new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                pasoC++;
                paso = pasoC;
                mLayout1.addView(createNewTextView(mEditText1.getText().toString()));
                pasos2[pasoC] =  add;
                mEditText1.setText("");
            }
        };
    }

    final ArrayList list1 = new ArrayList();
    final ArrayList list2 = new ArrayList();
    final ArrayList list3 = new ArrayList();

    public String[] pasos1 = new String[19];
    public String[] pasos2 = new String[19];
    public String[] pasos3 = new String[19];


    private View.OnClickListener onClick3() {
        return new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                pasoCo++;
                paso = pasoCo;
                mLayout2.addView(createNewTextView(mEditText2.getText().toString()));

                pasos3[pasoCo] = add;
                mEditText2.setText("");
            }
        };
    }
    public String add;
    int paso = 0;

    private TextView createNewTextView(String text) {
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        final TextView textView = new TextView(this);

        textView.setLayoutParams(lparams);
        textView.setText("-Paso "+paso+": " + text);
        add = textView.getText().toString();


        return textView;
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}

