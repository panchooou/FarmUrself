package com.example.franciscojavier.farmurself.Adaptadores;

import android.content.Intent;
import android.widget.BaseAdapter;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.franciscojavier.farmurself.Clases.Cultivo;
import com.example.franciscojavier.farmurself.InfoCultivos.InfoCultActivity;
import com.example.franciscojavier.farmurself.R;

public class GridAdapter extends BaseAdapter {
    private final Context mContext;
    private final Cultivo[] items;

    public GridAdapter(Context c, Cultivo[] items){
        mContext = c;
        this.items = items;
    }

    @Override
    public int getCount(){
        //Decremento en 1, para no contar el header view
        return items.length;
    }
    @Override
    public Cultivo getItem(int position){
        return items[position];
    }
    @Override
    public long getItemId(int position){
        return 0;
    }
    @Override
    public View getView(int position, View view, ViewGroup viewGroup){
        if (view == null){
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.grid_item, viewGroup, false);
        }
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, InfoCultActivity.class);
                mContext.startActivity(intent);
            }
        });

        Cultivo item = getItem(position);

        //Setear la imagen
        ImageView image = (ImageView) view.findViewById(R.id.imagen);
        Glide.with(image.getContext()).load(item.getImage()).into(image);

        //Setear el nombre
        TextView name = (TextView)view.findViewById(R.id.nombre);
        name.setText(item.getNombre());

        //Setear el detalle
        //TextView detalle = (TextView)view.findViewById(R.id.detalle);
        //detalle.setText(item.getDetalle());

        return view;
    }

}
