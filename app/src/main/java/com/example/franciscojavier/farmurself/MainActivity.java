package com.example.franciscojavier.farmurself;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {
    Button btn_Registrar, btnIniciarFace, btn_iniciarSesion;
    EditText username, password;
    CheckBox recordar;
    JSONArray ja;
    CallbackManager callbackManager;
    LoginButton loginButton;
    TextView txt;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Iniciar Fb SDK
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_main);

        //Establecer las devoluciones de llamada
        callbackManager = CallbackManager.Factory.create();
        loginButton = (LoginButton)findViewById(R.id.btnIniciarFace);

        btn_iniciarSesion = (Button)findViewById(R.id.btn_iniciarSesion);
        btn_Registrar = (Button)findViewById(R.id.btn_Registrar);
        username = (EditText)findViewById(R.id.username);
        password = (EditText)findViewById(R.id.password);
        recordar = (CheckBox)findViewById(R.id.recordar);




        //Registrar las devoluciones de llamadas de sdk
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.i("LOGIN", "Todo bien");
               // txt.setText("User ID: "+ loginResult.getAccessToken().getUserId()+ "n" +"Auth Token: "+ loginResult.getAccessToken().getToken());
               // makeToast(loginResult.getAccessToken().toString());
                IrAMenu();
            }

            @Override
            public void onCancel() {
                Log.i("LOGIN", "Se ha cancelado el inicio");
                Toast.makeText(getApplicationContext(), "Se canceló el inicio de sesión", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Log.i("LOGIN","Se ah producido un error.");
                String errorMessage = "Login error.";
                txt.setText(errorMessage);
                makeToast(errorMessage);
            }
        });

        //txt = (TextView)findViewById(R.id.txt1);

        //Si está logeado en facebook esto hace que pase inmediato al menu principal
        if(isLoggedIn()) {
            IrAMenu();
           // txt.setText("User IDxxxx: " + AccessToken.getCurrentAccessToken().getUserId());

        }





        btn_iniciarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConsultarPassword("http://192.168.50.2" +
                        "/login.php?user="+username.getText().toString());

            }
        });

        btn_Registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentRegister = new Intent(MainActivity.this, Registro.class);
                MainActivity.this.startActivity(intentRegister);
            }
        });
}

    public boolean isLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return (accessToken != null) && (!accessToken.isExpired());

    }

    private void makeToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show();
    }

    public void IrAMenu() {

        Intent intent = new Intent(MainActivity.this, DrawerActivity.class);
       // intent.putExtra("var", txt.getText());
        startActivity(intent);
        finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void ConsultarPassword(String URL) {

    Log.i("url",""+URL);
    RequestQueue queue = Volley.newRequestQueue(this);
    StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
        @Override
        public void onResponse(String response) {
            try {
                ja = new JSONArray(response);
                String pass = ja.getString(0);
                if (pass.equals(password.getText().toString())) {
                    Toast.makeText(getApplicationContext(), "Bienvenido", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(MainActivity.this, DrawerActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "Verifique que su contraseña está correcta", Toast.LENGTH_SHORT).show();
                }


            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), "El usuario no existe en la base de datos", Toast.LENGTH_LONG).show();
            }
        }
    }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Toast.makeText(getApplicationContext(), "No se realiza ninguna accion", Toast.LENGTH_LONG).show();
        }
    });
    queue.add(stringRequest);
}
    public static Bitmap getFacebookProfilePicture(String userID) throws SocketException, SocketTimeoutException, MalformedURLException, IOException, Exception
    {
        String imageURL;

        Bitmap bitmap = null;
        imageURL = "http://graph.facebook.com/"+userID+"/picture?type=large";
        InputStream in = (InputStream) new URL(imageURL).getContent();
        bitmap = BitmapFactory.decodeStream(in);

        return bitmap;
    }


}

